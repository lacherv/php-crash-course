<?php
    /* ----------- Arrrays ----------- */

    /*
    If you need to store multiple values, you can use arrays. Arrays hold "elements"
    */

    // Simple array of numbers
    $numbers = [1, 2, 3, 4, 5];

    // Simple array of strings
    // $colors = ['red', 'blue', 'green'];

    // Outputting 
    // print_r($colors);
    // var_dump($numbers);

    // Outputting values from an array
    // echo $numbers[0];
    // echo $numbers[3] + $numbers[4];

    /* ------ Associative Arrays ----- */

    /*
        Associative arrays allow us to use named keys to identify values.
    */

    $colors = [
        1 => 'red',
        2 => 'green',
        3 => 'blue',
    ];

    // echo $colors[3];
    // Strings as keys
    $hex = [
        'red' => '#f00',
        'green' => '#0f0',
        'blue' => '#00f',
    ];
    
    // echo $hex['red'];
    // var_dump($hex);

    $person = [
        'first_name' => 'Brad',
        'last_name' => 'Traversy',
        'email' => 'brad@gmail.com'
    ];

    // echo $person['email'];

    /* ---- Multi-dimensional arrays ---- */

    /*
        Multi-dimansional arrays are often used to store data in a table format.
    */

    $people = [
        [
            'first_name' => 'Brad',
            'last_name' => 'Traversy',
            'email' => 'brad@gmail.com'
        ],
        [
            'first_name' => 'Johan',
            'last_name' => 'Doe',
            'email' => 'joe@gmail.com'
        ],
        [
            'first_name' => 'Jane',
            'last_name' => 'Doe',
            'email' => 'jane@gmail.com'
        ],
    ];
    // var_dump($people);

    // Accessing values in a multi-dimensional array
    // echo $people[0]['first_name'];
    // echo $people[2]['email'];

    // Encode to JSON
    // print_r(json_encode($people));
    // var_dump((json_encode($people));

    // Decode from JSON
    $jsonobj = '{"first_name":"Brad","last_name": "Traversy","email":"brad@gmail.com"}';
    var_dump(json_decode($jsonobj));
